from setuptools import setup, find_packages

test_deps = [
        "pillow",
        "stable-baselines3 @ git+https://github.com/carlosluis/stable-baselines3@fix_tests",
        "mujoco",
        ]

extras = {
             'test': test_deps,
         }

setup(
    name="march_gym",
    version="0.0.2",
    install_requires=["gym",
                      "pillow",
                      "imageio",
                      "mathparser @ git@gitlab.com:project-march/libraries/mathparser.git"],

    extras_require=extras,

    package_data={
            "march_gym": [
                "envs/assets/*.xml",
                "envs/assets/meshes/*.stl",
                "envs/assets/meshes/*.obj"
            ]
        },
    packages=find_packages(),
)
