

# MARCH Gym
This is a fork from [Gym Examples](https://github.com/Farama-Foundation/gym-examples) which has simple examples of Gym environments and wrappers.
For some explanations of these examples, see the [Gym documentation](https://www.gymlibrary.ml).

### Install

```shell
pip install git+https://gitlab.com/project-march/Human/march_gym
```

### Developing

(Recommended) Make a virtual environment
```shell
python3 -m venv .gymenv
source .gymenv/bin/activate
```

1. Clone the directory:
```shell
git clone https://gitlab.com/project-march/Human/march_gym
git checkout dev
```

2. Install the environment:
```shell
cd march_gym
pip install -e .
```

3. To install the dependencies needed to test MARCH Gym, use:
```shell
pip install .[test]
```
