from gym.envs.registration import register

register(
    id="March-v0",
    entry_point="march_gym.envs:MarchEnv",
    max_episode_steps=1000,
)