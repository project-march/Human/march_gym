import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    tau_max = 100
    tau_f = np.logspace(-3, 3, 7)

    b = 50
    print(tau_f)

    omega_max_predicted = (tau_max - tau_f) / b
    omega_max_predicted[-1] = 0
    print(np.round(omega_max_predicted,2 ))

    omega_max_true = np.array((1.99998, 1.9998, 1.998, 1.98, 1.95953, 1.95953, 1.95953))

    plt.semilogx(tau_f, omega_max_predicted, label='Predicted')
    plt.semilogx(tau_f, omega_max_true, '+--', label='True')
    plt.xlabel(r'$\tau_\mathrm{f}$ [N m]')
    plt.ylabel(r'$\omega_{\mathrm{max}}$[rad s$^{-1}]$')
    plt.grid()
    plt.legend()
    plt.show()