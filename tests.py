import unittest
from io import StringIO

import yaml

from march_gym.tools import xml_generator


#
# class MarchGymTestCase(unittest.TestCase):
#
#     def test_env_creation(self):
#         env = gym.make('March-v0')
#
#     def test_env_using_stable_baselines(self):
#         from stable_baselines3.common.env_checker import check_env
#         env = gym.make('March-v0')
#
#         check_env(env)
#
#     def test_env_render_to_image(self):
#         env = gym.make('March-v0', render_mode='rgb_array', camera_name='left')
#         env.reset()
#
#         img = Image.fromarray(env.render())
#
#         plt.imshow(img)
#         plt.show()
#         self.assertIsInstance(img, PIL.Image.Image)
#
#     def test_use_XML_input(self):
#         env = gym.make('March-v0', xml_file='test_assets/test.xml')
#

class XMLGeneratorTestCase(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.YAML_FILE = StringIO()

        data = {
            'phys': {
                'pelvis': {
                    'pos': {
                        'x': -0.02,
                        'y': 0,
                        'z': 0.13},
                    'size': {
                        'x': 0.051,
                        'y': 0.189,
                        'z': 0.165},
                    'mass' : 6.3,
                    'com': {
                        'x': -0.02,
                        'y': 0,
                        'z': 0.16},
                    'inertia': {
                        'xx': 0.121,
                        'yy': 0.06,
                        'zz': 0.077,
                        'xy': 0,
                        'xz': -0.001,
                        'yz': 0.001
                    },
                }
            },
            'joint': {
                'HFE' : {
                    'max_speed' : 20,
                    'max_torque' : 120,
                    'inertia' : 20
                },
                'KFE': {
                    'max_speed': 40,
                    'max_torque': -30,
                    'inertia': 10
                }
            }
        }

        yaml.safe_dump(data, self.YAML_FILE, default_flow_style=False, encoding=None)
        self.YAML_FILE.seek(0)

    def test_read_yaml(self):
        gen = xml_generator.XMLGenerator(mark_compile_time=False)
        data = gen._read_yaml(self.YAML_FILE)

        self.assertEqual(data['phys']['pelvis']['pos']['x'], -0.02)
        self.assertEqual(data['phys']['pelvis']['pos']['y'], 0)
        self.assertEqual(data['phys']['pelvis']['pos']['z'], 0.13)

        self.assertEqual(data['phys']['pelvis']['pos'], {'x': -0.02,
                                                         'y': 0,
                                                         'z': 0.13})

    def test_generate_xml_easy(self):

        pre_xml = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="${phys/pelvis/com/x ./y ./z}" mass="${phys/pelvis/mass}" \n\
                fullinertia="${phys/pelvis/inertia/xx ./yy ./zz ./xy ./xz ./yz}"/> \n\
                <geom name="backpack" type="box" \n\
                    size="${phys/pelvis/size/x ./y ./z}" \n\
                    pos="${phys/pelvis/pos/x ./y ./z}" class="collision"/> ')

        out = StringIO()

        truth = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="-0.02 0 0.16" mass="6.3" \n\
                fullinertia="0.121 0.06 0.077 0 -0.001 0.001"/> \n\
                <geom name="backpack" type="box" \n\
                    size="0.051 0.189 0.165" \n\
                    pos="-0.02 0 0.13" class="collision"/> ')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_with_absolutes(self):

        pre_xml = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="${phys/pelvis/com/x phys/pelvis/com/y phys/pelvis/com/z}" \
                mass="${phys/pelvis/mass}" \n\
                fullinertia="${phys/pelvis/inertia/xx} ${phys/pelvis/inertia/yy phys/pelvis/inertia/zz \
                phys/pelvis/inertia/xy phys/pelvis/inertia/xz phys/pelvis/inertia/yz}"/> \n\
                <geom name="backpack" type="box" \n\
                    size="${phys/pelvis/size/x ./y ./z}" \n\
                    pos="${phys/pelvis/pos/x ./y ./z}" class="collision"/> ')

        out = StringIO()

        truth = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="-0.02 0 0.16" \
                mass="6.3" \n\
                fullinertia="0.121 0.06 0.077 \
                0 -0.001 0.001"/> \n\
                <geom name="backpack" type="box" \n\
                    size="0.051 0.189 0.165" \n\
                    pos="-0.02 0 0.13" class="collision"/> ')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_single_negative(self):

        pre_xml = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="${phys/pelvis/com/x ./y ./z}" mass="${phys/pelvis/mass}" \n\
                fullinertia="${-phys/pelvis/inertia/xx ./yy ./zz ./xy ./xz -./yz}"/> \n\
                <geom name="backpack" type="box" \n\
                    size="${-phys/pelvis/size/x -./y -./z}" \n\
                    pos="${phys/pelvis/pos/x ./y ./z}" class="collision"/> ')

        out = StringIO()

        truth = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="-0.02 0 0.16" mass="6.3" \n\
                fullinertia="-0.121 0.06 0.077 0 -0.001 -0.001"/> \n\
                <geom name="backpack" type="box" \n\
                    size="-0.051 -0.189 -0.165" \n\
                    pos="-0.02 0 0.13" class="collision"/> ')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_double_negative(self):

        pre_xml = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="${-phys/pelvis/com/x ./y ./z}" mass="${phys/pelvis/mass}" \n\
                fullinertia="${-phys/pelvis/inertia/xx ./yy ./zz ./xy ./xz -./yz}"/> \n\
                <geom name="backpack" type="box" \n\
                    size="${-phys/pelvis/size/x -./y -./z}" \n\
                    pos="${-phys/pelvis/pos/x ./y ./z}" class="collision"/> ')

        out = StringIO()

        truth = StringIO(
            '<body name="march_pelvis" pos="0 0 1" childclass="exoskeleton"> \n\
                <inertial pos="0.02 0 0.16" mass="6.3" \n\
                fullinertia="-0.121 0.06 0.077 0 -0.001 -0.001"/> \n\
                <geom name="backpack" type="box" \n\
                    size="-0.051 -0.189 -0.165" \n\
                    pos="0.02 0 0.13" class="collision"/> ')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_division(self):

        pre_xml = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="${joint/HFE/inertia}" damping="${joint/HFE/max_torque / ./max_speed}"  frictionloss="0" stiffness="0"/> \n\
                <joint xyz="${(joint/HFE/max_torque / ./max_speed) ./max_torque ./max_torque}" \n\
                </default>')

        out = StringIO()

        truth = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="20" damping="6.0"  frictionloss="0" stiffness="0"/> \n\
                <joint xyz="6.0 120.0 120.0" \n\
                </default>')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_addition(self):

        pre_xml = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="${joint/HFE/inertia + joint/KFE/inertia + ./inertia + joint/KFE/inertia}"/> \n\
                </default>')

        out = StringIO()

        truth = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="60.0"/> \n\
                </default>')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)

    def test_generate_xml_double_neg(self):

        pre_xml = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="${-joint/KFE/max_torque - joint/HFE/inertia - joint/KFE/max_torque + ./inertia + joint/KFE/max_torque}"/> \n\
                </default>')

        out = StringIO()

        truth = StringIO(
            '<default class="rotational_joint"> \n\
                <joint armature="20.0"/> \n\
                </default>')

        gen = xml_generator.XMLGenerator(mark_compile_time=False)

        data = gen._read_yaml(self.YAML_FILE)
        gen._main(pre_xml, data, out)
        out.seek(0)
        pre_xml.seek(0)

        for line_out, line_truth in zip(out, truth):
            line_out = line_out.strip()
            line_truth = line_truth.strip()
            print(f'{line_out} Should equal -> \n{line_truth}\n\n')
            self.assertEqual(line_out, line_truth)



if __name__ == '__main__':
    unittest.main()
